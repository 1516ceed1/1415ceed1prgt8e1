/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt8e1.modelo;

import java.util.Date;

/**
 * Fichero: Alumno.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Alumno {

   private String id;
   private String nombre;
   private int edad;
   private String email;
   private Date fecha;
   private Grupo grupo;

   public Alumno(String id_, String nombre_, int edad_, String email_) {
      id = id_;
      nombre = nombre_;
      edad = edad_;
      email = email_;
   }

   public Alumno() {
      nombre = "";
      edad = 0;
   }

   /**
    * @return the nombre
    */
   public String getNombre() {
      return nombre;
   }

   /**
    * @param nombre the nombre to set
    */
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

   /**
    * @return the edad
    */
   public int getEdad() {
      return edad;
   }

   /**
    * @param edad the edad to set
    */
   public void setEdad(int edad) {
      this.edad = edad;
   }

   /**
    * @return the email
    */
   public String getEmail() {
      return email;
   }

   /**
    * @param email the email to set
    */
   public void setEmail(String email) {
      this.email = email;
   }

   /**
    * @return the id
    */
   public String getId() {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(String id) {
      this.id = id;
   }

   public String toString() {
      return getId() + " " + getNombre() + " " + getEdad() + " "
              + getEmail() + " " + this.getGrupo().getId() + " " + this.getGrupo().getNombre();
   }

   /**
    * @return the grupo
    */
   public Grupo getGrupo() {
      return grupo;
   }

   /**
    * @param grupo the grupo to set
    */
   public void setGrupo(Grupo grupo) {
      this.grupo = grupo;
   }

   public Date getFecha() {
      return fecha;
   }

   public void setFecha(Date fecha) {
      this.fecha = fecha;
   }

}
