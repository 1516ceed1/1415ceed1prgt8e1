/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt8e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.ceedcv.ceed1prgt8e1.vista.VistaGraficaAcerca;

/**
 *
 * @author paco
 */
class ControladorAcerca implements ActionListener {

   final static String SALIR = "Salir";
   VistaGraficaAcerca vista;

   public ControladorAcerca(VistaGraficaAcerca vista) {
      this.vista = vista;
      inicializa();
   }

   private void salir() {
      vista.setVisible(false);
   }

   private void inicializa() {
      vista.getjbSalir().setActionCommand(SALIR);
      vista.getjbSalir().addActionListener(this);
      vista.setVisible(true);
      try {
         vista.setMaximum(true);
         vista.setSelected(true);
      } catch (java.beans.PropertyVetoException e) {
      }
   }

   @Override
   public void actionPerformed(ActionEvent e) {
      String comando = e.getActionCommand();

      switch (comando) {
         case SALIR:
            salir();
            break;
      }
   }

}
