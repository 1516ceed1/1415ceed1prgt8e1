package org.ceedcv.ceed1prgt8e1.controlador;

import java.io.IOException;
import org.ceedcv.ceed1prgt8e1.modelo.IModelo;
import org.ceedcv.ceed1prgt8e1.modelo.ModeloMysql;
import org.ceedcv.ceed1prgt8e1.vista.VistaGrafica;
import org.ceedcv.ceed1prgt8e1.vista.VistaMenuPrincipal;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 22-feb-2016
 */
public class Main {

    public static void main(String[] args) throws IOException {

        IModelo modelo = new ModeloMysql();
        //VistaGrafica vista = new VistaGrafica();
        //Controlador controlador = new Controlador(modelo, vista);
        VistaMenuPrincipal vista = new VistaMenuPrincipal();
        Controlador controlador = new Controlador(modelo, vista);

    }

}
