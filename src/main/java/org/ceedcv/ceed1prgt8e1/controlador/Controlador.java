package org.ceedcv.ceed1prgt8e1.controlador;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ceedcv.ceed1prgt8e1.modelo.IModelo;
import org.ceedcv.ceed1prgt8e1.modelo.ModeloMysql;
import org.ceedcv.ceed1prgt8e1.vista.VistaGraficaAcerca;
import org.ceedcv.ceed1prgt8e1.vista.VistaGraficaAlumno;
import org.ceedcv.ceed1prgt8e1.vista.VistaGraficaGrupo;
import org.ceedcv.ceed1prgt8e1.vista.VistaMenuPrincipal;
import org.ceedcv.ceed1prgt8e1.vista.VistaPortada;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Controlador implements ActionListener {

   private IModelo modelo;

   //VistaGrafica vista;
   private VistaMenuPrincipal vista;
   private VistaGraficaGrupo vistaGrupo;
   private VistaGraficaAlumno vistaAlumno;
   private VistaGraficaAcerca vistaAcerca;
   private VistaPortada vistaPortada;

   private ControladorAlumno controladorAlumno;
   private ControladorGrupo controladorGrupo;
   private ControladorAcerca controladorAcerca;

   final static String ALUMNO = "ALUMNO";
   final static String GRUPO = "GRUPO";
   final static String WEB = "WEB";
   final static String ACERCA = "ACERCA";
   final static String PDF = "PDF";
   final static String INSTALAR = "INSTALAR";
   final static String PORTADA = "PORTADA";
   final static String SALIR = "SALIR";

   Controlador(IModelo modelo_, VistaMenuPrincipal vista_) {
      vista = vista_;
      modelo = modelo_;
      inicializa();
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      String comando = e.getActionCommand();

      switch (comando) {
         case SALIR:
            salir();
            break;
         case ALUMNO:
            alumno();
            break;
         case GRUPO:
            grupo();
            break;
         case WEB:
            web();
            break;
         case PDF:
            pdf();
            break;
         case INSTALAR:
            instalar();
            break;
         case ACERCA:
            acerca();
            break;
      }

   }

   private void salir() {
      vista.setVisible(false);
      System.exit(0);
   }

   private void inicializa() {

      vista.getAlumno().setActionCommand(ALUMNO);
      vista.getGrupo().setActionCommand(GRUPO);
      vista.getWeb().setActionCommand(WEB);
      vista.getAcerca().setActionCommand(ACERCA);
      vista.getInstalar().setActionCommand(INSTALAR);
      vista.getPdf().setActionCommand(PDF);
      vista.getSalir().setActionCommand(SALIR);

      vista.getAlumno().addActionListener(this);
      vista.getGrupo().addActionListener(this);
      vista.getWeb().addActionListener(this);
      vista.getAcerca().addActionListener(this);
      vista.getInstalar().addActionListener(this);
      vista.getPdf().addActionListener(this);
      vista.getSalir().addActionListener(this);

      vista.setTitle("VISTA PRINCIPAL");
      vista.setLocationRelativeTo(null); // Centrar
      portada();
      vista.setVisible(true);

   }

   private void portada() {
      vistaPortada = new VistaPortada();
      vista.getEscritorio().add(vistaPortada);
      vistaPortada.setTitle(PORTADA);
      vistaPortada.setVisible(true);
      try {
         vistaPortada.setMaximum(true);
         vistaPortada.setSelected(true);
      } catch (java.beans.PropertyVetoException e) {
      }
   }

   private void acerca() {
      vistaAcerca = new VistaGraficaAcerca();
      vistaAcerca.setTitle(ACERCA);
      vista.getEscritorio().add(vistaAcerca);
      controladorAcerca = new ControladorAcerca(vistaAcerca);
   }

   private void alumno() {
      vistaAlumno = new VistaGraficaAlumno();
      vistaAlumno.setTitle(ALUMNO);
      vista.getEscritorio().add(vistaAlumno);
      controladorAlumno = new ControladorAlumno(vistaAlumno, modelo);
   }

   private void grupo() {
      vistaGrupo = new VistaGraficaGrupo();
      vistaGrupo.setTitle(GRUPO);
      vista.getEscritorio().add(vistaGrupo);
      controladorGrupo = new ControladorGrupo(vistaGrupo, modelo);

   }

   private void pdf() {
      try {
         String filename = "docu.pdf";
         File doc = new File(filename);
         Desktop.getDesktop().open(doc);
      } catch (IOException ex) {
         Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
      }
   }

   private void web() {

      String url = "https://docs.google.com/document/d/1BpbwyFPjpAvHCqed5i2txtw8b0r6V4G99V9s-PKkIfM/edit?usp=sharing";

      Desktop desktop = Desktop.getDesktop();

      if (desktop.isSupported(Desktop.Action.BROWSE)) {
         try {
            desktop.getDesktop().browse(new URI(url));
         } catch (URISyntaxException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
         }
      } else {
         pdf();
      }
   }

   private void instalar() {

      String error = null;

      ModeloMysql msql = new ModeloMysql();
      error = msql.createbd();
      if (error != null) {
         org.ceedcv.ceed1prgt8e1.vista.Funciones.mostarTexto(vista, error);
         return;
      }

      error = msql.creartablas();
      if (error != null) {
         org.ceedcv.ceed1prgt8e1.vista.Funciones.mostarTexto(vista, error);
         return;
      }

      org.ceedcv.ceed1prgt8e1.vista.Funciones.mostarTexto(vista, "Instalación correcta");

   }

}
