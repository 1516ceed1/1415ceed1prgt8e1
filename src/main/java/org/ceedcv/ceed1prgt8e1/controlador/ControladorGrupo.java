/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt8e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed1prgt8e1.modelo.Grupo;
import org.ceedcv.ceed1prgt8e1.modelo.IModelo;
import org.ceedcv.ceed1prgt8e1.vista.VistaGraficaGrupo;

/**
 *
 * @author paco
 */
public class ControladorGrupo implements ActionListener {

   private IModelo modelo;
   private VistaGraficaGrupo vista;
   private ArrayList grupos;
   private Grupo actual;
   private int iactual;
   private String operacion;

   final static String GCREATE = "CREATE";
   final static String GREAD = "READ";
   final static String GUPDATE = "UPDATE";
   final static String GDELETE = "DELETE";
   final static String GSALIR = "SALIR";

   final static String GPRIMERO = "PRIMERO";
   final static String GULTIMO = "ULTIMO";
   final static String GSIGUIENTE = "SIGUIENTE";
   final static String GANTERIOR = "ANTERIOR";
   final static String GGUARDAR = "GUARDAR";
   final static String GCANCELAR = "CANCELAR";

   ControladorGrupo(VistaGraficaGrupo vista, IModelo modelo) {

      this.vista = vista;
      this.modelo = modelo;
      inicializa();
   }

   private void inicializa() {

      inicializaNavegables();
      inicializaCrud();
      vista.setTitle("GRUPO");
      vista.setVisible(true);
      try {
         vista.setMaximum(true);
         vista.setSelected(true);
      } catch (java.beans.PropertyVetoException e) {
      }
      grupos = modelo.readg();
      if (grupos.size() > 0) {
         primero();
      }
      mostrar(actual);
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      String comando = e.getActionCommand();
      switch (comando) {
         case GPRIMERO:
            primero();
            mostrar(actual);
            break;
         case GULTIMO:
            ultimo();
            mostrar(actual);
            break;
         case GSIGUIENTE:
            siguiente();
            mostrar(actual);
            break;

         case GANTERIOR:
            anterior();
            mostrar(actual);
            break;

         case GSALIR: // Exit
            salir();
            break;

         case GCREATE: // Create
            operacion = GCREATE;
            vista.getjtfId().setText("");
            vista.getjtfNombre().setText("");

            vista.getjtfId().setEditable(false);
            vista.getjtfId().setEnabled(false);
            vista.getjtfNombre().setEditable(true);

            activaCRUD(false);
            break;

         case GGUARDAR:
            guardar(operacion);
            mostrar(actual);
            operacion = "";
            break;

         case GCANCELAR:
            mostrar(actual);
            operacion = "";
            break;

         case GREAD: // Read
            operacion = GREAD;
            vista.getjtfId().setText("");
            vista.getjtfNombre().setText("");

            vista.getjtfId().setEditable(true);
            vista.getjtfNombre().setEditable(false);

            activaCRUD(false);
            vista.getjtfId().setFocusable(true);

            break;

         case GUPDATE:  // Actualizar
            operacion = GUPDATE;
            vista.getjtfId().setEditable(true);
            vista.getjtfNombre().setEditable(true);
            activaCRUD(false);

            break;

         case GDELETE: // Borrar
            operacion = GDELETE;
            activaCRUD(false);
            break;

      }

   }

   private void salir() {
      vista.setVisible(false);
   }

   private void mostrar(Grupo primero) {

      editarCampos(false);
      activaCRUD(true);

      if (primero == null) {
         return;
      }

      vista.getjtfId().setText(primero.getId());
      vista.getjtfNombre().setText(primero.getNombre());

   }

   public void activaCRUD(Boolean b) {
      vista.getjbGuardar().setEnabled(!b);
      vista.getjbCancelar().setEnabled(!b);
      vista.getjbSalir().setEnabled(b);
      vista.getjbRead().setEnabled(b);
      vista.getjbUpdate().setEnabled(b);
      vista.getjbDelete().setEnabled(b);
      vista.getjbCreate().setEnabled(b);
   }

   private Grupo obtener() {
      Grupo grupo = new Grupo();

      String id = vista.getjtfId().getText();
      String nombre = vista.getjtfNombre().getText();

      grupo.setId(id);
      grupo.setNombre(nombre);

      return grupo;
   }

   private void getGrupo(String id) {

      Boolean encontrado = false;
      Grupo grupo = null;

      int talla = grupos.size();
      int i = 0;

      while (i < talla && encontrado == false) {
         grupo = (Grupo) grupos.get(i);
         if (grupo.getId().equals(id)) {
            encontrado = true;
            iactual = i;
         }
         i++;
      }

      if (encontrado == true) {
         actual = grupo;
      } else {
         iactual = 0;
         actual = (Grupo) grupos.get(iactual);
      }
   }

   private void anterior() {
      if (iactual != 0) {
         iactual--;
         actual = (Grupo) grupos.get(iactual);
      }
   }

   private void siguiente() {
      if (iactual != grupos.size() - 1) {
         iactual++;
         actual = (Grupo) grupos.get(iactual);
      }
   }

   private void ultimo() {
      iactual = grupos.size() - 1;
      actual = (Grupo) grupos.get(iactual);
   }

   private void primero() {

      if (grupos != null) {
         actual = (Grupo) grupos.get(iactual);
         iactual = 0;
      } else {
         actual = null;
         iactual = -1;
      }

   }

   private void inicializaNavegables() {
      vista.getjbPrimero().setActionCommand(GPRIMERO);
      vista.getjbUltimo().setActionCommand(GULTIMO);
      vista.getjbSiguiente().setActionCommand(GSIGUIENTE);
      vista.getjbAnterior().setActionCommand(GANTERIOR);
      vista.getjbPrimero().addActionListener(this);
      vista.getjbUltimo().addActionListener(this);
      vista.getjbSiguiente().addActionListener(this);
      vista.getjbAnterior().addActionListener(this);
   }

   private void inicializaCrud() {
      vista.getjbCreate().setActionCommand(GCREATE);
      vista.getjbRead().setActionCommand(GREAD);
      vista.getjbUpdate().setActionCommand(GUPDATE);
      vista.getjbDelete().setActionCommand(GDELETE);
      vista.getjbSalir().setActionCommand(GSALIR);
      vista.getjbCreate().addActionListener(this);
      vista.getjbRead().addActionListener(this);
      vista.getjbUpdate().addActionListener(this);
      vista.getjbDelete().addActionListener(this);
      vista.getjbSalir().addActionListener(this);

      vista.getjbGuardar().setActionCommand(GGUARDAR);
      vista.getjbCancelar().setActionCommand(GCANCELAR);
      vista.getjbGuardar().addActionListener(this);
      vista.getjbCancelar().addActionListener(this);
   }

   private void guardar(String operacion) {

      Grupo grupo = new Grupo();
      switch (operacion) {
         case GCREATE:
            grupo = obtener();
            modelo.create(grupo);
            grupos.add(grupo);
            actual = grupo;
            iactual = grupos.size() - 1;
            break;
         case GDELETE:
            grupo = obtener();
            modelo.delete(grupo);
            grupos = modelo.readg();
            anterior();
            break;
         case GUPDATE:  // Actualizar
            grupo = obtener();
            modelo.update(grupo);
            actual = grupo;
            grupos = modelo.readg();
            break;
         case GREAD:
            grupo = obtener();
            String id = grupo.getId();
            getGrupo(id);
            break;
      }
   }

   private void editarCampos(boolean b) {
      vista.getjtfId().setEditable(b);
      vista.getjtfNombre().setEditable(b);
   }

}
