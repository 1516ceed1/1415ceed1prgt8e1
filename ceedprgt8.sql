-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-02-2016 a las 08:01:03
-- Versión del servidor: 5.5.47-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ceedprgt8`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `ida` int(11) NOT NULL AUTO_INCREMENT,
  `nombrea` varchar(25) NOT NULL,
  `edad` int(11) NOT NULL,
  `email` varchar(25) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`ida`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `alumnos`
--
UPDATE `1516ceedprg`.`alumnos` SET `edad` = '45' WHERE `alumnos`.`ida` =2;


INSERT INTO `alumnos` (`ida`, `nombrea`, `edad`, `email`, `fecha`) VALUES
(1, 'Paco Aldarias', 25, 'paco.aldarias@ceedcv.es', '0000-00-00'),
(2, 'a', 1, 'a@a.es', '2016-02-26'),
(3, 'aa', 21, 'a@ab.es', '2016-02-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `idg` int(5) NOT NULL AUTO_INCREMENT,
  `nombreg` varchar(35) NOT NULL,
  PRIMARY KEY (`idg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`idg`, `nombreg`) VALUES
(2, 'asd'),
(3, 'sdaf'),
(4, 'safdfasdf');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
